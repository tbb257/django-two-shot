from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = ("name", "id")


@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = ("name", "number", "id")


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "id",
    )
