from .views import show_receipts, create_receipts, show_categories
from .views import show_accounts, create_category, create_account
from django.urls import path

urlpatterns = [
    path("", show_receipts, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/", show_categories, name="category_list"),
    path("accounts/", show_accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
